package eclipseLauncher;

import java.io.*;
import java.nio.file.*;
import java.util.regex.*;
import java.util.stream.*;

import javax.swing.*;

public class Main {

  private static final boolean DEBUG = false;
  
  public static final String SETTING = "eclipseLauncher.setting";
  
  public static final String DELETE = "delete";
  
  public static void main(String[]args) {
    new Main().execute(args);
  }
      
  public void execute(String[]args) {   
    getKeyValues().forEach(kv-> {
      if (DEBUG) System.out.println("" + kv);
      if (kv.key.equals("delete")) delete(kv.value);
      if (kv.key.equals("launch")) launch(kv.value);
    });
  }
  
  private void delete(String spec) {
    char divideChar = spec.charAt(0);
    spec = spec.substring(1);
    int patEnd = spec.indexOf(divideChar);
    if (patEnd < 0) {
      error("No pattern end character");
      return;
    }
    String patString = spec.substring(0, patEnd);
    Path dirPath = Paths.get(spec.substring(patEnd + 1));
    if (!Files.isDirectory(dirPath)) {
      error("Not a directory:" + dirPath);
      return;
    }
    Pattern pattern;
    try {
      pattern = Pattern.compile(patString);
    } catch (Exception ex) {
      error("Invalid pattern:" + patString);
      return;
    }
    try {
      Files.list(dirPath).forEach(filePath-> {
        String filename = filePath.getFileName().toString();
        if (DEBUG) System.out.println(filename);
        if (pattern.matcher(filename).matches()) {
          if (DEBUG) System.out.println("matches:" + filename);          
          try {
            NioFiles.delete(filePath);
          } catch (IOException ex) {
            error("Could not delete:" + filePath);
          }          
        }
      });
    } catch (IOException ex) {
      error("Could not get file list:" + dirPath);
    }
  }
  
  private void launch(String spec) {
    Path path = Paths.get(spec);
    ProcessBuilder p = new ProcessBuilder();
    p.directory(path.getParent().toFile());
    p.command(spec);
    try {
      p.start();
    } catch (Exception ex) {
      error(ex.getMessage());
    }
    System.exit(0);
  }

  Stream<KeyValue> getKeyValues() {    
    Path baseDir = Paths.get(System.getProperty("user.dir"));       
    Path setting = baseDir.resolve(SETTING);
    try {
      return Files.readAllLines(setting).stream().map(line-> {
        int e = line.indexOf('=');
        if (e < 0) return null;
        return new KeyValue(line.substring(0, e).trim(), line.substring(e + 1).trim());
      }).filter(o->o != null);      
    } catch (IOException ex) {
      JOptionPane.showMessageDialog(null,  "Could not read:" + setting); 
      System.exit(1);
      return null;
    }    
  }
  
  private void error(String message) {
    JOptionPane.showMessageDialog(null,  message); 
    System.exit(1);
  }
  
  static class KeyValue {
    final String key;
    final String value;
    KeyValue(String key, String value) {
      this.key = key;
      this.value = value;
    }
    @Override
    public String toString() {
      return key + "=" + value;
    }
  }
}
