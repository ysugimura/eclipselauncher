package eclipseLauncher;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;

public class NioFiles {

  public static void mkdirs(Path path) throws IOException {
    Files.createDirectories(path);
  }
  
  /**
   * 指定されたファイルまたはディレクトリを削除する。ディレクトリの場合は、それ以下もすべて削除される。
   * @param target
   * @throws IOException
   */
  public static void delete(Path target) throws IOException {
    if (!Files.exists(target)) return;
    if (Files.isRegularFile(target)) {
      deleteOne(target);
      return;
    }
    Files.walkFileTree(target, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        deleteOne(file);
        return FileVisitResult.CONTINUE;
      }
      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        deleteOne(dir);
        return FileVisitResult.CONTINUE;
      }
   });
  }

  /** 単一のファイルもしくはディレクトリを削除。ディレクトリの場合は空であること。readonlyになっていても削除する。 */
  private static void deleteOne(Path path) throws IOException {    
    if (!Files.isWritable(path)) {
      // nio的ではないが、面倒なのでこちらを使う。
      path.toFile().setWritable(true);
    }
    Files.delete(path);
  }

}
